#include<bsd/string.h>
#include<stdio.h>
#include<stdlib.h>

// Linux code requires:
// sudo apt-get install pkg-config clang libbsd-dev 
// to compile:
// clang -Wall -Wextra -pedantic -fsanitize=leak cpystr.c $(pkg-config --libs libbsd)


#define LEN_DST1 6

int cpyString(const char * source,
              char* destination)
{
    if (!source || !destination)
    {
        printf("Error: Invalid arguments passed to function cpyString.\n");
        return -1;
    }

    if (sizeof(source) > sizeof(destination)){
        destination = realloc(destination, sizeof(source) + 1);
    }

    size_t len = strlcpy(destination, source, sizeof(destination));

    if (len >= sizeof(destination))
    {
        printf("Error: Attempted to copy string of length %zu (including null"
               " terminator), into a destination buffer of size %zu."
               " Truncation has occurred.\n",
               len + 1,
               sizeof(destination));
    }
    else
    {
        printf("String copied successfully\n");
    }
    free(destination);
    return len;
}

int main (void)
{
    const char src1[] = "Alfoo";
    char dst1 = (char *) malloc(sizeof(char) * 3);

    cpyString(src1, dst1);
    puts(dst1);
    return 0;
}
