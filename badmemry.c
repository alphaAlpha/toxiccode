#include <stdlib.h>
#include <stdio.h>

int main(void)
{
    // Static memory on the stack
    int a = 0;

    // Dynamic memory on the heap
    // malloc()
    int *ip = malloc(sizeof(int));          //allocate memory for one int
    int *arr = malloc(sizeof(int) * 50);    //allocate memory for 50 ints

    // malloc() can fail
    if (!ip || !arr)
    {
        return -1;
    }

    *ip = 42;
    arr[49] = 0xB0B;
    arr[50] = 0xBAAAAD;

    // free()
    free(arr);;
    arr = NULL;
    free(arr);

    // calloc()
    double *darr;
    // calloc() takes 2 args: number of elements, sizeof each element
    // initialises everything to 0
    darr = calloc(100, sizeof(double)); // same as malloc(sizeof(double) * 100);
    if(!darr)
    {
        return -1;
    }

    // realloc()
    darr = realloc(darr, sizeof(double) * 50);

    free(darr);
    darr = NULL;

    return 0;
}
